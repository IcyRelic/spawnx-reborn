package me.icyrelic.com.Listeners;

import java.util.Set;

import me.icyrelic.com.SpawnX;
import me.icyrelic.com.Data.Updates;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class SpawnListener implements Listener{
	SpawnX plugin;
	public SpawnListener(SpawnX instance) {

		plugin = instance;

		}
	
	@EventHandler
	public void onPlayerDeath(PlayerRespawnEvent e){
		Player p = e.getPlayer();
		if(plugin.getConfig().getBoolean("SpawnX.MultiWorldSpawns") == true){
			if(plugin.getConfig().contains("SpawnX.Spawns."+p.getWorld().getName())){
				p.teleport(getSpawnLocation(p.getWorld()));
			}else{
				p.sendMessage(ChatColor.RED + "No Spawnpoint in This World");
			}
			
		}else{
			//1 world spawn
			if(plugin.getConfig().contains("SpawnX.Spawns")){
				Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
				String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
				String[] str = spawnsstr.split(", ");
				
				p.teleport(getSpawnLocation(plugin.getServer().getWorld(str[0])));
				
			}else{
				p.sendMessage(ChatColor.RED + "No Spawnpoint");
			}

		}
		
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();

		
		if(plugin.getConfig().getBoolean("SpawnX.Update_Notification")){
			if(p.hasPermission("SpawnX.Notify")){
				Updates.checkVersion("Version: " + plugin.getDescription().getVersion());
				if(p.isOp() || p.hasPermission("SpawnX.Notice")){
					  if(Updates.updates){
						  p.sendMessage("["+ChatColor.RED + "SpawnX Reborn"+ChatColor.WHITE+"]"+" New Version Available");
						  p.sendMessage("["+ChatColor.RED + "SpawnX Reborn"+ChatColor.WHITE+"]"+" Download Here: ");
						  p.sendMessage(ChatColor.GRAY+"http://dev.bukkit.org/bukkit-plugins/spawnx-reborn/");
					  }
				  }
			}
		}
		
		if(plugin.getConfig().getBoolean("SpawnX.Users." + p.getName() + "_Joined") == false || plugin.getConfig().getString("SpawnX.Users." + p.getName() + "_Joined") == null){
			
			if(plugin.getConfig().getBoolean("SpawnX.NewPlayerSpawn") == true){
				//send player to noob spawn
				if(plugin.getConfig().contains("SpawnX.NoobSpawn")){
					p.teleport(getNoobSpawn());
				}else{
					p.sendMessage(ChatColor.RED + "New player spawn not set");
				}
			}else{
				//send player to normal spawn
				if(plugin.getConfig().getBoolean("SpawnX.MultiWorldSpawns") == true){
					if(plugin.getConfig().contains("SpawnX.Spawns."+p.getWorld().getName())){
						p.teleport(getSpawnLocation(p.getWorld()));
					}else{
						p.sendMessage(ChatColor.RED + "No Spawnpoint in This World");
					}
				}else{
					//1 world spawn
					if(plugin.getConfig().contains("SpawnX.Spawns")){
						Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
						String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
						String[] str = spawnsstr.split(", ");
						
						p.teleport(getSpawnLocation(plugin.getServer().getWorld(str[0])));
						
					}else{
						p.sendMessage(ChatColor.RED + "No Spawnpoint");
					}

				}
				
				
	          
			}
			
			  plugin.getConfig().set("SpawnX.Users." + p.getName() + "_Joined", true);
	          plugin.saveConfig();
			
		}
		
		
		
		  
	}
	
	public Location getNoobSpawn(){
		
		
		String world = plugin.getConfig().getString("SpawnX.NoobSpawn.world");
		
		World w = plugin.getServer().getWorld(world);
		
		String loc = plugin.getConfig().getString("SpawnX.NoobSpawn.location");
		Float pitch = Float.parseFloat(plugin.getConfig().getString("SpawnX.NoobSpawn.pitch"));
		Float yaw = Float.parseFloat(plugin.getConfig().getString("SpawnX.NoobSpawn.yaw"));
		String[] xyz = loc.split(", ");
		
       	final Location SpawnLoc = new Location(w, Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]));
        SpawnLoc.setPitch(pitch);
        SpawnLoc.setYaw(yaw);
        SpawnLoc.add(0.5, 0, 0.5);
        return SpawnLoc;
	}
	
	public Location getSpawnLocation(World w){
		
		Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
		String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
		String[] str = spawnsstr.split(", ");
		
		String world = str[0];
		
		String loc = plugin.getConfig().getString("SpawnX.Spawns."+world+".location");
		Float pitch = Float.parseFloat(plugin.getConfig().getString("SpawnX.Spawns."+world+".pitch"));
		Float yaw = Float.parseFloat(plugin.getConfig().getString("SpawnX.Spawns."+world+".yaw"));
		String[] xyz = loc.split(", ");
		
       	final Location SpawnLoc = new Location(w, Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]));
        SpawnLoc.setPitch(pitch);
        SpawnLoc.setYaw(yaw);
        SpawnLoc.add(0.5, 0, 0.5);
        return SpawnLoc;
	}
	
}
