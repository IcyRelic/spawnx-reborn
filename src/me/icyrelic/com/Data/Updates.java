package me.icyrelic.com.Data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Updates {
	public static boolean updates = false;
	
	public static void checkVersion(String s){
		
		 try {
			 URL url = new URL("http://icyrelic.com/plugins/versions/SpawnX_Reborn.html");

			 BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			 String str;
			 while ((str = in.readLine()) != null) {
				 if(str.equals(s)){
					 updates = false;
				 }else{
					 updates = true;
				 }
			}
			 
			in.close();
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
	}

}
