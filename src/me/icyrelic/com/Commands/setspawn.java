package me.icyrelic.com.Commands;

import java.util.Set;

import me.icyrelic.com.SpawnX;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class setspawn implements CommandExecutor {
	
	SpawnX plugin;
	public setspawn(SpawnX instance) {

		plugin = instance;

		}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		  final Player p = (Player) sender;
		  String noPerm = (ChatColor.RED + "You Dont Have Permission!");
		
		if (cmd.getName().equalsIgnoreCase("setspawn")) {
			
			if(p.hasPermission("SpawnX.setspawn")){
				if(plugin.getConfig().getBoolean("SpawnX.MultiWorldSpawns") == true){
					String world = p.getWorld().getName();
					String location = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
					Float pitch = (p.getLocation().getPitch());
					Float yaw = (p.getLocation().getYaw());
					if(plugin.getConfig().contains("SpawnX.Spawns."+world)){
						p.sendMessage(ChatColor.RED + "Spawnpoint Moved in World '" + ChatColor.GREEN + world + ChatColor.RED + "'");
					}else{
						p.sendMessage(ChatColor.RED + "Spawnpoint Set in World '" + ChatColor.GREEN + world + ChatColor.RED + "'");
					}
					
					plugin.getConfig().set("SpawnX.Spawns."+world+".location", location);
					plugin.getConfig().set("SpawnX.Spawns."+world+".pitch", pitch);
					plugin.getConfig().set("SpawnX.Spawns."+world+".yaw", yaw);
					plugin.saveConfig();
				}else{
					//1 world spawn
					if(plugin.getConfig().contains("SpawnX.Spawns")){
						Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
						String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
						String[] str = spawnsstr.split(", ");
						String world = str[0];
						
						String location = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
						Float pitch = (p.getLocation().getPitch());
						Float yaw = (p.getLocation().getYaw());
						
						if(plugin.getConfig().contains("SpawnX.Spawns."+world)){
							p.sendMessage(ChatColor.RED + "Spawnpoint Moved in World '" + ChatColor.GREEN + world + ChatColor.RED + "'");
						}else{
							p.sendMessage(ChatColor.RED + "Spawnpoint Set in World '" + ChatColor.GREEN + world + ChatColor.RED + "'");
						}
						
						plugin.getConfig().set("SpawnX.Spawns."+world+".location", location);
						plugin.getConfig().set("SpawnX.Spawns."+world+".pitch", pitch);
						plugin.getConfig().set("SpawnX.Spawns."+world+".yaw", yaw);
						plugin.saveConfig();
						
					}else{
						
						String world = p.getWorld().getName();
						String location = (p.getLocation().getBlockX()+", "+p.getLocation().getBlockY()+", "+p.getLocation().getBlockZ());
						Float pitch = (p.getLocation().getPitch());
						Float yaw = (p.getLocation().getYaw());
						if(plugin.getConfig().contains("SpawnX.Spawns."+world)){
							p.sendMessage(ChatColor.RED + "Spawnpoint Moved in World '" + ChatColor.GREEN + world + ChatColor.RED + "'");
						}else{
							p.sendMessage(ChatColor.RED + "Spawnpoint Set in World '" + ChatColor.GREEN + world + ChatColor.RED + "'");
						}
						
						plugin.getConfig().set("SpawnX.Spawns."+world+".location", location);
						plugin.getConfig().set("SpawnX.Spawns."+world+".pitch", pitch);
						plugin.getConfig().set("SpawnX.Spawns."+world+".yaw", yaw);
						plugin.saveConfig();
						
						
					}

				}
			}else{
				p.sendMessage(noPerm);
			}
			

			
		}
		
		
		
		
		
	return true;
	}
	
	
	
	
	public Location getSpawnLocation(World w){
		
		Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
		String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
		String[] str = spawnsstr.split(", ");
		
		String world = str[0];
		
		String loc = plugin.getConfig().getString("SpawnX.Spawns."+world+".location");
		Float pitch = Float.parseFloat(plugin.getConfig().getString("SpawnX.Spawns."+world+".pitch"));
		Float yaw = Float.parseFloat(plugin.getConfig().getString("SpawnX.Spawns."+world+".yaw"));
		String[] xyz = loc.split(", ");
		
       	final Location SpawnLoc = new Location(w, Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]));
        SpawnLoc.setPitch(pitch);
        SpawnLoc.setYaw(yaw);
        SpawnLoc.add(0.5, 0, 0.5);
        return SpawnLoc;
	}
	
	

}
