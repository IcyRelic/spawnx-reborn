package me.icyrelic.com.Commands;

import java.util.Set;

import me.icyrelic.com.SpawnX;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class spawn implements CommandExecutor {
	
	SpawnX plugin;
	public spawn(SpawnX instance) {

		plugin = instance;

		}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		  final Player p = (Player) sender;
		  String noPerm = (ChatColor.RED + "You Dont Have Permission!");
		  

		
		if (cmd.getName().equalsIgnoreCase("spawn")) {
			
			  if(p.hasPermission("SpawnX.spawn")){
					boolean delay = plugin.getConfig().getBoolean("SpawnX.plugin.Teleport_Delay.Enabled");
					boolean cooldown = plugin.getConfig().getBoolean("SpawnX.Cooldown.Enabled");
					boolean multiworld = plugin.getConfig().getBoolean("SpawnX.MultiWorldSpawns");
					
					if(cooldown){
						 int cooldownTime = plugin.getConfig().getInt("SpawnX.Cooldown.Cooldown"); // Get number of seconds from wherever you want
					        if(!p.hasPermission("SpawnX.cooldown.bypass")){
					        	if(plugin.cooldowns.containsKey(sender.getName())) {
						            long secondsLeft = ((plugin.cooldowns.get(sender.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
						            if(secondsLeft>0) {
						                // Still cooling down
						                sender.sendMessage("You cant use that commands for another "+ secondsLeft +" seconds!");
						                return true;
						            }
						        }
					        }
					        // No cooldown found or cooldown has expired, save new cooldown
					        plugin.cooldowns.put(sender.getName(), System.currentTimeMillis());					
					}
					
					if(delay && !p.hasPermission("SpawnX.delay.bypass")){
		        		if(multiworld){
							if(plugin.getConfig().contains("SpawnX.Spawns."+p.getWorld().getName())){
								plugin.dTeleport(getSpawnLocation(p.getWorld()), p);
							}else{
								p.sendMessage(ChatColor.RED + "No Spawnpoint in This World");
							}
						}else{
							//1 world spawn
							if(plugin.getConfig().contains("SpawnX.Spawns")){
								Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
								String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
								String[] str = spawnsstr.split(", ");
								
								plugin.dTeleport(getSpawnLocation(plugin.getServer().getWorld(str[0])), p);
								
							}else{
								p.sendMessage(ChatColor.RED + "No Spawnpoint");
							}

						}
					}else{
		        		if(multiworld){
							if(plugin.getConfig().contains("SpawnX.Spawns."+p.getWorld().getName())){
								plugin.Teleport(getSpawnLocation(p.getWorld()), p);
							}else{
								p.sendMessage(ChatColor.RED + "No Spawnpoint in This World");
							}
						}else{
							//1 world spawn
							if(plugin.getConfig().contains("SpawnX.Spawns")){
								Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
								String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
								String[] str = spawnsstr.split(", ");
								
								plugin.Teleport(getSpawnLocation(plugin.getServer().getWorld(str[0])), p);
								
							}else{
								p.sendMessage(ChatColor.RED + "No Spawnpoint");
							}

						}
					}
					
					
					
			  }else{
				  p.sendMessage(noPerm);
			  }

			
			
			

		}
		
		
		
		
		
	return true;
	}
	
	
	
	
	public Location getSpawnLocation(World w){
		
		boolean multiworld = plugin.getConfig().getBoolean("SpawnX.MultiWorldSpawns");
		
		String worldName = "";
		
		if(multiworld){
			worldName = w.getName();
		}else{
			Set<String> spawns = plugin.getConfig().getConfigurationSection("SpawnX.Spawns").getKeys(false);
			String spawnsstr = spawns.toString().replace("[", "").replace("]", "");
			String[] str = spawnsstr.split(", ");
			
			worldName = str[0];
		}
		
		String loc = plugin.getConfig().getString("SpawnX.Spawns."+worldName+".location");
		Float pitch = Float.parseFloat(plugin.getConfig().getString("SpawnX.Spawns."+worldName+".pitch"));
		Float yaw = Float.parseFloat(plugin.getConfig().getString("SpawnX.Spawns."+worldName+".yaw"));
		String[] xyz = loc.split(", ");
		
       	final Location SpawnLoc = new Location(w, Integer.parseInt(xyz[0]), Integer.parseInt(xyz[1]), Integer.parseInt(xyz[2]));
        SpawnLoc.setPitch(pitch);
        SpawnLoc.setYaw(yaw);
        SpawnLoc.add(0.5, 0, 0.5);
        return SpawnLoc;
	}
	
	

}
