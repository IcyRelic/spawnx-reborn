package me.icyrelic.com;


import java.util.HashMap;

import me.icyrelic.com.Commands.setnoobspawn;
import me.icyrelic.com.Commands.setspawn;
import me.icyrelic.com.Commands.spawn;
import me.icyrelic.com.Data.Updates;
import me.icyrelic.com.Listeners.SpawnListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SpawnX extends JavaPlugin{
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	 
	public void onEnable() {
		Updates.checkVersion("Version: " + getDescription().getVersion());
		loadConfiguration();
		Bukkit.getServer().getPluginManager().registerEvents(new SpawnListener(this), this);
		getCommand("spawn").setExecutor(new spawn(this));
		getCommand("setspawn").setExecutor(new setspawn(this));
		getCommand("setnoobspawn").setExecutor(new setnoobspawn(this));
		
	}
	
	
	
	


	
	public void Teleport(Location spawn, Player p){
		cooldowns.put(p.getName(), System.currentTimeMillis());
		boolean msge = getConfig().getBoolean("SpawnX.Teleport_Message.Enabled");
        String msg = getConfig().getString("SpawnX.Teleport_Message.Message");
        if(msge){
        	p.sendMessage(msg.replaceAll("(&([a-f0-9]))", "\u00A7$2"));
        }
        
        p.teleport(spawn);
		  
		  
	}
	
	public void dTeleport(final Location spawn, final Player p){
		int delays = getConfig().getInt("SpawnX.Teleport_Delay.Delay");
		p.sendMessage(ChatColor.GRAY + "Teleport will commence in " + delays + " Seconds...");
		  getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
		  public void run() {
			  Teleport(spawn, p);
		  }
		}, 20*delays);
		
	}

	public void loadConfiguration(){
	    getConfig().options().copyDefaults(true);

	    saveConfig();
	
	}
	
}